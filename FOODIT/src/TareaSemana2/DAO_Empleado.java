package TareaSemana2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class DAO_Empleado {

	//1
	private static final String Insertar_Empleado = "INSERT INTO EMPLEADOS (ID_EMPLEADO,CEDULA,NOMBRE,APELLIDO,SUELDO,ID_SUCURSAL) VALUES (?,?,?,?,?,?)";
	//2
	private static final String Empleado_PorID = "SELECT * FROM EMPLEADOS WHERE ID_EMPLEADO =?";
	//3
	private static final String Todos_Los_Empleados = "SELECT * FROM EMPLEADOS";
	//4
	private static final String Modificar_Empleado = "UPDATE EMPLEADOS SET NOMBRE = ?, APELLIDO = ?, SUELDO= ? WHERE ID_EMPLEADO = ? AND ID_SUCURSAL = ?";
	//5
	private static final String Borrar_Empleado = "DELETE FROM EMPLEADOS WHERE ID_EMPLEADO =?";
	
	// 1) Insertar empleado
	public static boolean insert (Empleado InsertarEmpleado) {
		try {
			PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Insertar_Empleado);
			statement.setInt(1,InsertarEmpleado.getIdEmpleado());
			statement.setString(2,InsertarEmpleado.getCedula());
			statement.setString(3,InsertarEmpleado.getNombre());
			statement.setString(4,InsertarEmpleado.getApellido());
			statement.setInt(5,InsertarEmpleado.getSueldo());
			statement.setInt(6,InsertarEmpleado.getIdSucursalEmpleado());
			
			int retorno = statement.executeUpdate();
			return retorno>0;
			
		} catch (SQLException errorInsertar) {
			errorInsertar.printStackTrace();
			return false;
		}
	}
	
	// 2) Buscar empleado por ID
	public static Empleado find(int IdEmpleado) {
		try {
			PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Empleado_PorID);
			statement.setInt(1,IdEmpleado);
			
			ResultSet resultado = statement.executeQuery();
			Empleado empleado = null;
			while (resultado.next()) {
				empleado = getEmpleadoFromResultSet(resultado);
				String empleadoBuscado = null;
				for (int i=1; i<3;i++) {
				int resultadoIdEmpleado = resultado.getInt(1);
				String resultadoCedula = resultado.getString(2);
				String resultadoNombre = resultado.getString(3);
				String resultadoApellido = resultado.getString(4);
				String resultadoSueldo = resultado.getString(5);
				String resultadoIdSucursal = resultado.getString(6);
				empleadoBuscado = resultadoIdEmpleado + "," + resultadoCedula + "," + resultadoNombre + "," + resultadoApellido + "," + resultadoSueldo + "," + resultadoIdSucursal;
				}
				System.out.println("Datos encontrados: " + empleadoBuscado);
			}
			return empleado;
		} catch (SQLException errorBuscar) {
			errorBuscar.printStackTrace();
			return null;
		}
	}

	//Funcion auxiliar que traduce un registro a una instancia de empleado
	private static Empleado getEmpleadoFromResultSet(ResultSet resultado) throws SQLException {
		int IdEmpleado = resultado.getInt("ID_EMPLEADO");
		String cedula = resultado.getString("CEDULA");
		String nombre = resultado.getString("NOMBRE");
		String apellido = resultado.getString("APELLIDO");
		int sueldo = resultado.getInt("SUELDO");
		int IdSucursal = resultado.getInt("ID_SUCURSAL");
		
		Empleado empleadoBuscado = new Empleado (IdEmpleado, cedula, nombre, apellido, sueldo,IdSucursal);			
	return empleadoBuscado;
}
	// 3) Buscar todos los empleados
	public static LinkedList <Empleado> findAll(){
		LinkedList<Empleado> empleados = new LinkedList<>();
	try{
		PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Todos_Los_Empleados);
		ResultSet resultado = statement.executeQuery();
		
		while (resultado.next()){
			Empleado empleadoParaLista = getEmpleadoFromResultSet(resultado);
			empleados.add(empleadoParaLista );
			String empleadoBuscado = null;
			for (int i=1; i<3;i++) {
			int resultadoIdEmpleado = resultado.getInt(1);
			String resultadoCedula = resultado.getString(2);
			String resultadoNombre = resultado.getString(3);
			String resultadoApellido = resultado.getString(4);
			String resultadoSueldo = resultado.getString(5);
			String resultadoIdSucursal = resultado.getString(6);
			empleadoBuscado = resultadoIdEmpleado + "," + resultadoCedula + "," + resultadoNombre + "," + resultadoApellido + "," + resultadoSueldo + "," + resultadoIdSucursal;
			}
			System.out.println("Datos encontrados: " + empleadoBuscado);
	}
		return empleados;
	}	catch(SQLException errorEmpleados){
		errorEmpleados.printStackTrace();
		return null;
		}
	}

	// 4) Modificar Empleado 
	public static boolean edit (Empleado ModificarEmpleado) {
		try {
			PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Modificar_Empleado);
			statement.setString(1,ModificarEmpleado.getNombre());
			statement.setString(2,ModificarEmpleado.getApellido());
			statement.setInt(3,ModificarEmpleado.getSueldo());
			statement.setInt(4,ModificarEmpleado.getIdEmpleado());
			//statement.setString(2,ModificarEmpleado.getCedula());
			statement.setInt(5,ModificarEmpleado.getIdSucursalEmpleado());
			
			int retorno = statement.executeUpdate();
			return retorno>0;
			
		} catch (SQLException errorModificar) {
			errorModificar.printStackTrace();
			return false;
		}
	}
	
	// 5) Borrar empleado
		public static boolean delete (Empleado BorrarEmpleado) {
			try {
				PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Borrar_Empleado);
				statement.setInt(1,BorrarEmpleado.getIdEmpleado());

				int retorno = statement.executeUpdate();
				return retorno>0;
				
			} catch (SQLException errorBorrar) {
				errorBorrar.printStackTrace();
				return false;
			}
		}
}
