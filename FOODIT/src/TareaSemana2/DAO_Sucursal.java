package TareaSemana2;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class DAO_Sucursal {
	
	//1
	private static final String Insertar_Sucursal = "INSERT INTO SUCURSALES (ID_SUCURSAL,TELEFONO,DIRECCION,NOMBRE) VALUES (?,?,?,?)";
	//2
	private static final String Sucursal_PorID = "SELECT * FROM SUCURSALES WHERE ID_SUCURSAL =?";
	//3
	private static final String Todos_Las_Sucursales = "SELECT * FROM SUCURSALES";
	//4
	private static final String Modificar_Sucursal = "UPDATE SUCURSALES SET TELEFONO = ?, DIRECCION = ? WHERE ID_SUCURSAL = ?";
	//5
	private static final String Borrar_Sucursal = "DELETE FROM SUCURSALES WHERE ID_SUCURSAL =?";
		
	// 1) Insertar sucursal
		public static boolean insert (Sucursal InsertarSucursal) {
			try {
				PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Insertar_Sucursal);
				statement.setInt(1,InsertarSucursal.getIdSucursal());
				statement.setString(2,InsertarSucursal.getTelefono());
				statement.setString(3,InsertarSucursal.getDireccion());
				statement.setString(4,InsertarSucursal.getNombre());
				
				int retorno = statement.executeUpdate();
				return retorno>0;
				
			} catch (SQLException errorInsertar) {
				errorInsertar.printStackTrace();
				return false;
			}
		}
		
	// 2) Buscar sucursal por ID
		public static Sucursal find(int IdSucursal) {
			try {
				PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Sucursal_PorID);
				statement.setInt(1,IdSucursal);
				
				ResultSet resultado = statement.executeQuery();
				Sucursal sucursal = null;
				while (resultado.next()) {
					
					sucursal = getSucursalFromResultSet(resultado);
					String sucursalBuscada = null;
					for (int i=1; i<3;i++) {
					int resultadoIdSucursal = resultado.getInt(1);
					String resultadoTelefono = resultado.getString(2);
					String resultadoDireccion = resultado.getString(3);
					String resultadoNombre = resultado.getString(4);
					sucursalBuscada = resultadoIdSucursal + "," + resultadoTelefono + "," + resultadoDireccion + "," + resultadoNombre;
					}
					System.out.println("Datos encontrados: " + sucursalBuscada);
				}
				return sucursal;
			} catch (SQLException errorBuscar) {
				errorBuscar.printStackTrace();
				return null;
			}
		}
			

	//Funcion auxiliar que traduce un registro a una instancia de empleado
		private static Sucursal getSucursalFromResultSet(ResultSet resultado) throws SQLException {
			int IdSucursal = resultado.getInt("ID_SUCURSAL");
			String telefono = resultado.getString("TELEFONO");
			String direccion = resultado.getString("DIRECCION");
			String nombre = resultado.getString("NOMBRE");
				
			Sucursal sucursalBuscada = new Sucursal (IdSucursal, telefono, direccion, nombre);
		return sucursalBuscada;
	}
	// 3) Buscar todas las sucursales
		public static LinkedList <Sucursal> findAll(){
			LinkedList<Sucursal> sucursales = new LinkedList<>();
		try{
			PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Todos_Las_Sucursales);
			ResultSet resultado = statement.executeQuery();
			
			while (resultado.next()){
				Sucursal sucursalParaLista = getSucursalFromResultSet(resultado);
				sucursales.add(sucursalParaLista);
				
				String sucursalBuscada = null;
				for (int i=1; i<3;i++) {
				int resultadoIdSucursal = resultado.getInt(1);
				String resultadoTelefono = resultado.getString(2);
				String resultadoDireccion = resultado.getString(3);
				String resultadoNombre = resultado.getString(4);
				sucursalBuscada = resultadoIdSucursal + "," + resultadoTelefono + "," + resultadoDireccion + "," + resultadoNombre;
				}
				System.out.println("Sucursal: " + sucursalBuscada);
			}
			return sucursales;
		}	catch(SQLException errorSucursales){
			errorSucursales.printStackTrace();
			return null;
			}
		}

	// 4) Modificar Sucursal 
		public static boolean edit (Sucursal ModificarSucursal) {
			try {
				PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Modificar_Sucursal);
				statement.setInt(3,ModificarSucursal.getIdSucursal());
				statement.setString(1,ModificarSucursal.getTelefono());
				statement.setString(2,ModificarSucursal.getDireccion());
				
				int retorno = statement.executeUpdate();
				return retorno>0;
				
			} catch (SQLException errorModificar) {
				errorModificar.printStackTrace();
				return false;
			}
		}
		
	// 5) Borrar sucursal
			public static boolean delete (Sucursal BorrarSucursal) {
				try {
					PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(Borrar_Sucursal);
					statement.setInt(1,BorrarSucursal.getIdSucursal());

					int retorno = statement.executeUpdate();
					return retorno>0;
					
				} catch (SQLException errorBorrar) {
					errorBorrar.printStackTrace();
					return false;
				}
			}
	}
