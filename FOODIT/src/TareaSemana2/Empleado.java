package TareaSemana2;

public class Empleado {

	private int IdEmpleado;
	private String cedula;
	private String nombre;
	private String apellido;
	private int sueldo;
	private int IdSucursalEmpleado;
	
	public Empleado (int IdEmpleado, String cedula, String nombre,String apellido, int sueldo, int IdSucursalEmpleado){
		
		this.IdEmpleado = IdEmpleado;
		this.cedula = cedula;
		this.nombre = nombre;
		this.apellido = apellido;
		this.sueldo = sueldo;
		this.IdSucursalEmpleado = IdSucursalEmpleado;
	}

	public int getIdSucursalEmpleado() {
		return IdSucursalEmpleado;
	}

	public void setIdSucursalEmpleado(int idSucursalEmpleado) {
		IdSucursalEmpleado = idSucursalEmpleado;
	}

	public int getIdEmpleado() {
		return IdEmpleado;
	}

	public void setIdEmpleado(int IdEmpleado) {
		IdEmpleado = getIdEmpleado();
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getSueldo() {
		return sueldo;
	}

	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	
}
