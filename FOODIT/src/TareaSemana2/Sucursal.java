package TareaSemana2;

import java.util.LinkedList;

public class Sucursal {
	
		private int IdSucursal;
		private String telefono;
		private String direccion;
		private String nombre;
		
		public Sucursal (int IdSucursal, String telefono, String direccion,String nombre){
			
			this.IdSucursal = IdSucursal;
			this.telefono = telefono;
			this.direccion = direccion;
			this.nombre = nombre;	
		}

		public int getIdSucursal() {
			return IdSucursal;
		}

		public void setIdSucursal(int idSucursal) {
			IdSucursal = idSucursal;
		}

		public String getTelefono() {
			return telefono;
		}

		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}

		public String getDireccion() {
			return direccion;
		}

		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		LinkedList <Sucursal> ListaSucursal = new LinkedList <>();	
		public void addProducto(Sucursal sucursal) {
			ListaSucursal.add(sucursal);
		}
		
}
