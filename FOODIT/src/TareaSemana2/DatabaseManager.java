package TareaSemana2;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {

	private static Connection databaseConnection;
	
	private static String CONNECTION_STRING = "jdbc:oracle:thin:@localhost:1521:xe";
	private static String USUARIO = "FOODIT";
	private static String CLAVE = "FOODIT";
	
	static {
		try { 
			databaseConnection = DriverManager.getConnection(CONNECTION_STRING,USUARIO,CLAVE);
		} catch (SQLException error) {
			System.out.println("Error creando la conexi�n con la base de datos" + error);
		}
	}
	
	public static Connection getConnection () {
		return databaseConnection;
	}
}
